var gulp        = require('gulp'),
    stylus      = require('gulp-stylus'),
    nib         = require('nib')

var paths = {
    stylus: "stylus/*.styl",
    mainStylus: "stylus/style.styl",
    css: "css/style.css",
    dest: {
        css: "css"
    },
    build: {
        css: "dist/css"
    }
};


gulp.task('stylus', function () {

    return gulp.src(paths.mainStylus)
        .pipe(stylus({
            use: nib(),
            compress: true
        }))
        .pipe(gulp.dest(paths.dest.css))

});


gulp.task('watch', function(){
    gulp.watch(paths.stylus, ['stylus'])
});


gulp.task('default', [ 'stylus', 'watch']);